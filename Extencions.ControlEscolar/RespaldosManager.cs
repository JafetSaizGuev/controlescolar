﻿using Ionic.Zip;
using System;
using System.IO;

namespace Extencions.ControlEscolar
{
    public class RespaldosManager
    {
        public async void GenerarRespaldo(string ApplicationStartupPath,string Ruta)
        {
            try
            {
                var fecha = new DateTime();
                fecha = DateTime.Now;
                string nombreRespaldo = string.Format("ControlEscolar-{0}.zip", fecha.ToString("dd-MM-yyyy_HH_mm_ss"));
                string _logos = Path.Combine(ApplicationStartupPath, "logos");
                string _Archivos = Path.Combine(ApplicationStartupPath, "Archivos");
                string _Ruta = Path.Combine(Ruta, nombreRespaldo);
                DirectoryInfo dirLogo = new DirectoryInfo(_logos);
                DirectoryInfo dirArchivo = new DirectoryInfo(_Archivos);
                using (ZipFile zip = new ZipFile())
                {
                    if (dirLogo.Exists)
                    {
                        zip.AddDirectoryByName("logos");
                        zip.AddDirectory(_logos, "logos");
                    }
                    if (dirArchivo.Exists)
                    {
                        zip.AddDirectoryByName("Archivos");
                        zip.AddDirectory(_Archivos, "Archivos");
                    }
                    zip.Save(_Ruta);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async void RestaurarRespaldo(string ApplicationStartupPath,string Respaldo)
        {
            using (ZipFile zip = ZipFile.Read(Respaldo))
            {
                zip.ExtractAll(ApplicationStartupPath);
                zip.Dispose();
            }
        }
    }
}
