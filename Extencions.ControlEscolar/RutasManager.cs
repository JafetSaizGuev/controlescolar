﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.Devices;
using Entidades.ControlEscolar;
using System.Windows.Forms;

namespace Extencions.ControlEscolar
{
    public class RutasManager
    {
        #region Escuelas
        public bool GuardarEscuelas(bool Actualizar,string ApplicationStartupPath, string Archivo,Escuelas _Escuelas,string ArchivoViejo)
        {
            Computer C = new Computer();
            var ArchivoInfo = new FileInfo(Archivo);
            if (ArchivoInfo.Length < 10000)
            {
                try
                {
                    if (!Actualizar)
                    {
                        string ruta = Path.Combine(ApplicationStartupPath, "logos", _Escuelas.Nombre);
                        if (!Directory.Exists(ruta))
                            Directory.CreateDirectory(ruta);
                        ruta += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                        C.FileSystem.CopyFile(Archivo, ruta);
                    }
                    else
                    {
                        C.FileSystem.DeleteFile(ArchivoViejo);
                        string rutaA = Path.Combine(Application.StartupPath, "logos", _Escuelas.Nombre);
                        rutaA += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                        C.FileSystem.CopyFile(Archivo, rutaA);
                    }
                    _Escuelas.Logo = Archivo.Substring(Archivo.LastIndexOf(@"\"));
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error de ruta", MessageBoxButtons.OK);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("El tamaño de la imagen es mayor a 10 kilobit", "Error de tamaño en imagen", MessageBoxButtons.OK);
                return false;
            }
        }
        public void MostrarLogo(PictureBox ptbLogo,string ApplicationStartupPath,DataGridView dgvEscuelas)
        {
            ptbLogo.ImageLocation = Path.Combine(ApplicationStartupPath, "logos", dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString(), dgvEscuelas.CurrentRow.Cells["Logo"].Value.ToString());
        }
        public void EliminarRutaEscuela(string ApplicationStartupPath,DataGridView dgvEscuelas)
        {
            Computer C = new Computer();
            if (C.FileSystem.DirectoryExists(Path.Combine(ApplicationStartupPath, "logos", dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString())))
                C.FileSystem.DeleteDirectory(Path.Combine(ApplicationStartupPath, "logos", dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString()), Microsoft.VisualBasic.FileIO.DeleteDirectoryOption.DeleteAllContents);
        }
        #endregion
        
        #region Estudios
        public bool GuardarEstudio(bool actualizar,string ApplicationStartupPath,string Maestro, string Archivo,Estudios _Estudios)
        {
            Computer C = new Computer();
            try
            {
                if (!actualizar)
                {
                    string ruta = Path.Combine(ApplicationStartupPath, "Archivos", Maestro);
                    if (!Directory.Exists(ruta))
                        Directory.CreateDirectory(ruta);
                    ruta += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                    C.FileSystem.CopyFile(Archivo, ruta);
                }
                else
                {
                    string rutaD = Path.Combine(ApplicationStartupPath, "Archivos", _Estudios.MaestroID, _Estudios.Archivo);
                    C.FileSystem.DeleteFile(rutaD);
                    string rutaA = Path.Combine(ApplicationStartupPath, "Archivos", _Estudios.MaestroID);
                    rutaA += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                    C.FileSystem.CopyFile(Archivo, rutaA);
                    _Estudios.Archivo = Archivo.Substring(Archivo.LastIndexOf(@"\"));
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void EliminarEstudio(DataGridView dgvEstudios)
        {
            Computer C = new Computer();
            string ruta = Application.StartupPath + @"\Archivos\" + dgvEstudios.CurrentRow.Cells["MaestroID"].Value + @"\" + dgvEstudios.CurrentRow.Cells["Archivo"].Value;
            C.FileSystem.DeleteFile(ruta);
        }
        #endregion
    }
}
