﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Conexion
{
    public class ConexionBD
    {
        MySqlConnection _conn;
        public ConexionBD(string Server, string User, string Pass, string DB, uint Port)
        {
            MySqlConnectionStringBuilder cadena = new MySqlConnectionStringBuilder();
            cadena.Server = Server;
            cadena.UserID = User;
            cadena.Password = Pass;
            cadena.Database = DB;
            cadena.Port = Port;
            _conn = new MySqlConnection(cadena.ToString());
        }
        public void Ejecutar(string Cadena)
        {
            _conn.Open();
            MySqlCommand Cmm = new MySqlCommand(Cadena, _conn);
            Cmm.ExecuteNonQuery();
            _conn.Close();
        }
        public DataSet ObtenerDatos(String Cadena, String Tabla)
        {
            var ds = new DataSet();
            MySqlDataAdapter DA = new MySqlDataAdapter(Cadena, _conn);
            DA.Fill(ds, Tabla);
            return ds;
        }
    }
}
