﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class EscuelasManejador
    {
        private EscuelasAccesoDatos _EscuelasAccesoDatos;
        public EscuelasManejador()
        {
            _EscuelasAccesoDatos = new EscuelasAccesoDatos();
        }
        public void Guardar(Escuelas Escuela)
        {
            _EscuelasAccesoDatos.GuardarActualizar(Escuela);
        }
        public void Eliminar(int Id)
        {
            _EscuelasAccesoDatos.Eliminar(Id);
        }
        public List<Escuelas> Mostrar(string Filtro)
        {
            var list = new List<Escuelas>();
            list = _EscuelasAccesoDatos.Mostrar(Filtro);
            return list;
        }
    }
}
