﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class AlumnosManejador
    {
        private AlumnosAccesoDatos _AlumnosAccesoDatos;
        public AlumnosManejador()
        {
            _AlumnosAccesoDatos = new AlumnosAccesoDatos();
        }
        public void Guardar(Alumnos Alumno)
        {
            _AlumnosAccesoDatos.Guardar(Alumno);
        }
        public void Actualizar(Alumnos Alumno)
        {
            _AlumnosAccesoDatos.Actualizar(Alumno);
        }
        public void Eliminar(int NoControl)
        {
            _AlumnosAccesoDatos.Eliminar(NoControl);
        }
        public List<Alumnos> Mostrar(string Filtro)
        {
            var list = new List<Alumnos>();
            list = _AlumnosAccesoDatos.Mostrar(Filtro);
            return list;
        }
        public List<Alumnos> RegresarAlumnos()
        {
            var list = new List<Alumnos>();
            list = _AlumnosAccesoDatos.RegresarAlumnos();
            return list;
        }
    }
}
