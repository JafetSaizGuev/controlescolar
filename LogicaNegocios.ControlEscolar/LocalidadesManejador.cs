﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class EstadosManejador
    {
        private EstadosAccesoDatos _EstadosAccesoDatos;
        public EstadosManejador()
        {
            _EstadosAccesoDatos = new EstadosAccesoDatos();
        }
        public List<Estados> RegresarEstados()
        {
            var list = new List<Estados>();
            list = _EstadosAccesoDatos.MostrarEstados();
            return list;
        }
    }
    public class MunicipiosManejador
    {
        private MunicipiosAccesoDatos _MunicipiosAccesoDatos;
        public MunicipiosManejador()
        {
            _MunicipiosAccesoDatos = new MunicipiosAccesoDatos();
        }
        public List<Municipios> RegresarMunicipios(int filtro)
        {
            var list = new List<Municipios>();
            list = _MunicipiosAccesoDatos.MostrarMunicipios(filtro);
            return list;
        }
    }
}
