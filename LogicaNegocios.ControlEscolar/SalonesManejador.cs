﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class SalonesManejador
    {
        private SalonesAccesoDatos _SalonesAccesoDatos;
        public SalonesManejador()
        {
            _SalonesAccesoDatos = new SalonesAccesoDatos();
        }
        public void GuardarActualizar(Salones Salon)
        {
            _SalonesAccesoDatos.GuardarActualizar(Salon);
        }
        public void Eliminar(int ID)
        {
            _SalonesAccesoDatos.Eliminar(ID);
        }
        public List<Salones> Mostrar(string Grupo)
        {
            var list = new List<Salones>();
            list = _SalonesAccesoDatos.Mostrar(Grupo);
            return list;
        }
    }
}
