﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class MaestrosManejador
    {
        private MaestrosAccesoDatos _MaestrosAccesoDatos;
        public MaestrosManejador()
        {
            _MaestrosAccesoDatos = new MaestrosAccesoDatos();
        }
        public void guardar(Maestros Maestro)
        {
            _MaestrosAccesoDatos.Guardar(Maestro);
        }
        public void Actualizar(Maestros Maestro)
        {
            _MaestrosAccesoDatos.Actualizar(Maestro);
        }
        public void Eliminar(string MaestroCodigo)
        {
            _MaestrosAccesoDatos.Eliminar(MaestroCodigo);
        }
        public List<Maestros> Mostrar(string Codigo)
        {
            var list = new List<Maestros>();
            list = _MaestrosAccesoDatos.Mostrar(Codigo);
            return list;
        }
        public List<Maestros> RegresarMaestros(string Codigo)
        {
            var list = new List<Maestros>();
            list = _MaestrosAccesoDatos.RegresarMaestro(Codigo);
            return list;
        }
    }

    public class EstudiosManejador
    {
        private EstudiosAccesoDatos _EstudiosAccesoDatos;
        public EstudiosManejador()
        {
            _EstudiosAccesoDatos = new EstudiosAccesoDatos();
        }
        public void GuardarActualizar(Estudios Estudio)
        {
            _EstudiosAccesoDatos.GuardarActualizar(Estudio);
        }
        public void Eliminar(int DocID)
        {
            _EstudiosAccesoDatos.Eliminar(DocID);
        }
        public List<Estudios> Mostrar(string MaestroCodigo)
        {
            var list = new List<Estudios>();
            list = _EstudiosAccesoDatos.Mostrar(MaestroCodigo);
            return list;
        }
    }
}
