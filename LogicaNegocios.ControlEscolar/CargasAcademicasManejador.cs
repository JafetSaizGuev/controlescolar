﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class CargasAcademicasManejador
    {
        private CargasAcademicasAccesoDatos _CargasAcademicasAccesoDatos;
        public CargasAcademicasManejador()
        {
            _CargasAcademicasAccesoDatos = new CargasAcademicasAccesoDatos();
        }
        public void GuardarActualizar(CargasAcademicas CargaAcademica)
        {
            _CargasAcademicasAccesoDatos.GuardarActualizar(CargaAcademica);
        }
        public void Eliminar(int id)
        {
            _CargasAcademicasAccesoDatos.Eliminar(id);
        }
        public List<CargasAcademicas> Mostrar(string grupo)
        {
            var list = new List<CargasAcademicas>();
            list = _CargasAcademicasAccesoDatos.Mostrar(grupo);
            return list;
        }
    }
}
