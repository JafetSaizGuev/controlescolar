﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class MateriasManejador
    {
        private MateriasAccesoDatos _MateriasAccesoDatos;
        public MateriasManejador()
        {
            _MateriasAccesoDatos = new MateriasAccesoDatos();
        }
        public void Guardar(Materias Materia)
        {
            _MateriasAccesoDatos.Guardar(Materia);
        }
        public void Actualizar(Materias Materia)
        {
            _MateriasAccesoDatos.Actualizar(Materia);
        }
        public void Eliminar(string Codigo)
        {
            _MateriasAccesoDatos.Eliminar(Codigo);
        }
        public List<Materias> Mostrar(string Filtro)
        {
            var list = new List<Materias>();
            list = _MateriasAccesoDatos.Mostrar(Filtro);
            return list;
        }
        public List<Materias> RegresarMaterias()
        {
            var list = new List<Materias>();
            list = _MateriasAccesoDatos.RegresaMaterias();
            return list;
        }
    }
}
