﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class GruposManejador
    {
        private GruposAccesoDatos _GruposAccesoDatos;
        public GruposManejador()
        {
            _GruposAccesoDatos = new GruposAccesoDatos();
        }
        public void Guardar(Grupos Grupo)
        {
            _GruposAccesoDatos.Guardar(Grupo);
        }
        public void Actualizar(Grupos Grupo)
        {
            _GruposAccesoDatos.Actualizar(Grupo);
        }
        public void Eliminar(string ID)
        {
            _GruposAccesoDatos.Eliminar(ID);
        }
        public List<Grupos> Mostrar(string Filtro)
        {
            var list = new List<Grupos>();
            list = _GruposAccesoDatos.Mostrar(Filtro);
            return list;
        }
        public List<Grupos> RegresarGrupos()
        {
            var list = new List<Grupos>();
            list = _GruposAccesoDatos.RegresarGrupos();
            return list;
        }
    }
}
