﻿using AccesoDatos.ControlEscolar;
using Entidades.ControlEscolar;
using System.Collections.Generic;

namespace LogicaNegocios.ControlEscolar
{
    public class CarrerasManejador
    {
        private CarrerasAccesoDatos _CarrerasAccesoDatos;
        public CarrerasManejador()
        {
            _CarrerasAccesoDatos = new CarrerasAccesoDatos();
        }
        public void Guardar(Carreras Carrera)
        {
            _CarrerasAccesoDatos.Guardar(Carrera);
        }
        public void Actualizar(Carreras Carrera)
        {
            _CarrerasAccesoDatos.Actualizar(Carrera);
        }
        public void Eliminar(string Clave)
        {
            _CarrerasAccesoDatos.Eliminar(Clave);
        }
        public List<Carreras> Mostrar(string Filtro)
        {
            var list = new List<Carreras>();
            list = _CarrerasAccesoDatos.Mostrar(Filtro);
            return list;
        }
        public List<Carreras> RegresarCarreras()
        {
            var list = new List<Carreras>();
            list = _CarrerasAccesoDatos.RegresarCarrera();
            return list;
        }
    }
}
