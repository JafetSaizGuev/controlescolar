﻿using Conexion;
using Entidades.ControlEscolar;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstadosAccesoDatos
    {
        ConexionBD _conexion;
        public EstadosAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public List<Estados> MostrarEstados()
        {
            var list = new List<Estados>();
            string Consulta = "Select * from Estados";
            var ds = _conexion.ObtenerDatos(Consulta, "Estados");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Estado = new Estados()
                {
                    Estado = row["Estado"].ToString()
                };
                list.Add(Estado);
            }
            return list;
        }
    }
    public class MunicipiosAccesoDatos
    {
        ConexionBD _conexion;
        public MunicipiosAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public List<Municipios> MostrarMunicipios(int filtro)
        {
            var list = new List<Municipios>();
            string Consulta = string.Format("select * from municipios,estados_municipios where estados_municipios.municipios_id = municipios.id and estados_municipios.estados_id = {0}",filtro);
            var ds = _conexion.ObtenerDatos(Consulta, "municipios,estados_municipios");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Municipio = new Municipios()
                {
                    Municipio = row["Municipio"].ToString()
                };
                list.Add(Municipio);
            }
            return list;
        }
    }
}
