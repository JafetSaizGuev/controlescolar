﻿using Conexion;
using Entidades.ControlEscolar;
using System;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CargasAcademicasAccesoDatos
    {
        ConexionBD _conexion;
        public CargasAcademicasAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void GuardarActualizar(CargasAcademicas CargaAcademica)
        {
            string cadena = "";
            if (CargaAcademica.Id == 0)
                cadena = string.Format("insert into CargaAcademica values(null,'{0}','{1}')",
                CargaAcademica.Grupo, CargaAcademica.Materia);
            else
                cadena = string.Format("update CargaAcademica set Grupo ='{1}',Materia ='{2}' where id = {0}",
                CargaAcademica.Id,CargaAcademica.Grupo, CargaAcademica.Materia);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(int id)
        {
            string cadena = string.Format("delete from CargaAcademica where id = {0}",id);
            _conexion.Ejecutar(cadena);
        }
        public List<CargasAcademicas> Mostrar(string grupo)
        {
            var list = new List<CargasAcademicas>();
            string Consulta = string.Format("Select * from CargaAcademica where Grupo like '%{0}%'", grupo);
            var ds = _conexion.ObtenerDatos(Consulta, "CargaAcademica");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var cargaAcademica = new CargasAcademicas()
                {

                    Id = Convert.ToInt32(row["id"]),
                    Materia = row["Materia"].ToString(),
                    Grupo = row["Grupo"].ToString()
                };
                list.Add(cargaAcademica);
            }
            return list;
        }
    }
}
