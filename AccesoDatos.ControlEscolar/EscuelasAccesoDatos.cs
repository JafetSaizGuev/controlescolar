﻿using Conexion;
using Entidades.ControlEscolar;
using System;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelasAccesoDatos
    {
        ConexionBD _conexion;
        public EscuelasAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void GuardarActualizar(Escuelas Escuela)
        {
            if (Escuela.ID == 0)
            {
                string cadena = string.Format("insert into Escuelas values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                Escuela.Nombre,Escuela.Domicilio,Escuela.NumeroExterior,Escuela.Estado,Escuela.Municipio,Escuela.Telefono,Escuela.PaginaWeb,Escuela.DirectorActual,Escuela.Logo);
                _conexion.Ejecutar(cadena);
            }
            else
            {
                string cadena = string.Format("update Escuelas set Nombre='{1}',Domicilio='{2}',Numero_Exterior='{3}',Estado='{4}',Municipio='{5}',Telefono='{6}',Pagina_Web='{7}',Director_Actual='{8}',Logo='{9}' where id={0}",
                Escuela.ID,Escuela.Nombre, Escuela.Domicilio, Escuela.NumeroExterior, Escuela.Estado, Escuela.Municipio, Escuela.Telefono, Escuela.PaginaWeb, Escuela.DirectorActual,Escuela.Logo);
                _conexion.Ejecutar(cadena);
            }
        }
        public void Eliminar(int Id)
        {
            string cadena = string.Format("delete from Escuelas where id = {0}", Id);
            _conexion.Ejecutar(cadena);
        }
        public List<Escuelas> Mostrar(string Escul)
        {
            var list = new List<Escuelas>();
            string Consulta = string.Format("Select * from Escuelas where Nombre like '%{0}%'", Escul);
            var ds = _conexion.ObtenerDatos(Consulta, "Escuelas");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Escuela = new Escuelas()
                {
                    ID = Convert.ToInt32(row["id"]),
                    Nombre = row["Nombre"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                    NumeroExterior = row["Numero_Exterior"].ToString(),
                    PaginaWeb = row["Pagina_Web"].ToString(),
                    DirectorActual = row["Director_Actual"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Logo = row["Logo"].ToString()
                };
                list.Add(Escuela);
            }
            return list;
        }
    }
}
