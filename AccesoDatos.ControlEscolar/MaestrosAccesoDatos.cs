﻿using Conexion;
using Entidades.ControlEscolar;
using System;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MaestrosAccesoDatos
    {
        private ConexionBD _conexion;
        public MaestrosAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void Guardar(Maestros Maestro)
        {
            string cadena = string.Format("insert into Maestros values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')",
            Maestro.Codigo, Maestro.Nombre, Maestro.APaterno, Maestro.AMaterno,Maestro.Nacimiento,Maestro.Estado, Maestro.Municipio, Maestro.Sexo,Maestro.Correo, Maestro.Bancaria);
            _conexion.Ejecutar(cadena);
        }
        public void Actualizar(Maestros Maestro)
        {
            string cadena = string.Format("update Maestros set Nombre='{1}',APaterno='{2}',AMaterno='{3}',Nacimiento='{4}',Estado='{5}',Ciudad='{6}',Sexo='{7}',Correo='{8}',Bancaria='{9}' where Codigo='{0}'",
            Maestro.Codigo, Maestro.Nombre, Maestro.APaterno, Maestro.AMaterno, Maestro.Nacimiento, Maestro.Estado, Maestro.Municipio, Maestro.Sexo, Maestro.Correo, Maestro.Bancaria);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(string Codigo)
        {
            string cadena = string.Format("delete from Maestros where Codigo = '{0}'", Codigo);
            _conexion.Ejecutar(cadena);
        }
        public List<Maestros> Mostrar(string Codigo)
        {
            var list = new List<Maestros>();
            string Consulta = string.Format("Select * from Maestros where Codigo like '%{0}%'", Codigo);
            var ds = _conexion.ObtenerDatos(Consulta, "Maestros");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Maestro = new Maestros()
                {
                    Codigo = row["Codigo"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    APaterno = row["APaterno"].ToString(),
                    AMaterno = row["AMaterno"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    Nacimiento = row["Nacimiento"].ToString(),
                    Correo = row["Correo"].ToString(),
                    Bancaria = row["Bancaria"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Ciudad"].ToString()
                };
                list.Add(Maestro);
            }
            return list;
        }
        public List<Maestros> RegresarMaestro(string Codigo)
        {
            var list = new List<Maestros>();
            string Consulta = string.Format("Select * from Maestros where Codigo like '%{0}%'", Codigo);
            var ds = _conexion.ObtenerDatos(Consulta, "Maestros");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Maestro = new Maestros()
                {
                    Codigo = row["Codigo"].ToString(),
                    Nombre = row["Nombre"].ToString() + " " + row["APaterno"].ToString() + " " + row["AMaterno"].ToString()
                };
                list.Add(Maestro);
            }
            return list;
        }
    }
    public class EstudiosAccesoDatos
    {
        private ConexionBD _conexion;
        public EstudiosAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void GuardarActualizar(Estudios Estudio)
        {
            if (Estudio.Id == 0)
            {
                string cadena = string.Format("insert into Estudios values(null,'{1}','{2}')",
                Estudio.Id, Estudio.MaestroID, Estudio.Archivo);
                _conexion.Ejecutar(cadena);
            }
            else
            {
                string cadena = string.Format("update Estudios set Archivo='{1}' where Id={0}",Estudio.Id, Estudio.Archivo);
                _conexion.Ejecutar(cadena);
            }
        }
        public void Eliminar(int Doc)
        {
            string cadena = string.Format("delete from Estudios where Id = {0}", Doc);
            _conexion.Ejecutar(cadena);
        }
        public List<Estudios> Mostrar(string CodigoMaestro)
        {
            var list = new List<Estudios>();
            string Consulta = string.Format("Select * from Estudios where FK_Maestro = '{0}'", CodigoMaestro);
            var ds = _conexion.ObtenerDatos(Consulta, "Estudios");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Estudio = new Estudios()
                {
                    Id = Convert.ToInt32(row["id"]),
                    MaestroID = row["FK_Maestro"].ToString(),
                    Archivo = row["Archivo"].ToString()
                };
                list.Add(Estudio);
            }
            return list;
        }
    }
}
