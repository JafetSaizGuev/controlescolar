﻿using Conexion;
using Entidades.ControlEscolar;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class AlumnosAccesoDatos
    {
        ConexionBD _conexion;
        public AlumnosAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void Guardar(Alumnos Alumno)
        {
                string cadena = string.Format("insert into Alumnado values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')",
                Alumno.NoControl,Alumno.Nombre,Alumno.APaterno,Alumno.AMaterno,Alumno.Sexo,Alumno.Nacimiento,Alumno.Correo,Alumno.Telefono,Alumno.Estado,Alumno.Municipio,Alumno.Domicilio);
                _conexion.Ejecutar(cadena);
        }
        public void Actualizar(Alumnos Alumno)
        {
            string cadena = string.Format("update Alumnado set Nombre='{1}',APaterno='{2}',AMaterno='{3}',Sexo='{4}',Nacimiento='{5}',Correo='{6}',Telefono='{7}',Estado='{8}',Municipio='{9}',Domicilio='{10}' where NoControl='{0}'",
                Alumno.NoControl, Alumno.Nombre, Alumno.APaterno, Alumno.AMaterno, Alumno.Sexo, Alumno.Nacimiento, Alumno.Correo, Alumno.Telefono, Alumno.Estado, Alumno.Municipio, Alumno.Domicilio);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(int NoControl)
        {
            string cadena = string.Format("delete from Alumnado where NoControl = '{0}'", NoControl);
            _conexion.Ejecutar(cadena);
        }
        public List<Alumnos> Mostrar(string NoControl)
        {
            var list = new List<Alumnos>();
            string Consulta = string.Format("Select * from Alumnado where NoControl like '%{0}%'", NoControl);
            var ds = _conexion.ObtenerDatos(Consulta, "Alumnado");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Alumno = new Alumnos()
                {
                    NoControl = row["NoControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    APaterno = row["APaterno"].ToString(),
                    AMaterno = row["AMaterno"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    Nacimiento = row["Nacimiento"].ToString(),
                    Correo = row["Correo"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Domicilio = row["Domicilio"].ToString()
                };
                list.Add(Alumno);
            }
            return list;
        }
        public List<Alumnos> RegresarAlumnos()
        {
            var list = new List<Alumnos>();
            string Consulta = "Select * from Alumnado";
            var ds = _conexion.ObtenerDatos(Consulta, "Alumnado");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Alumno = new Alumnos()
                {
                    NoControl = row["NoControl"].ToString()
                };
                list.Add(Alumno);
            }
            return list;
        }
    }
}
