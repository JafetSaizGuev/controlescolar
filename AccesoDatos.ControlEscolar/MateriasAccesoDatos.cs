﻿using Conexion;
using Entidades.ControlEscolar;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MateriasAccesoDatos
    {
        ConexionBD _conexion;
        public MateriasAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void Guardar(Materias Materia)
        {
            string cadena;
            if(string.IsNullOrEmpty(Materia.Antecesora) && !string.IsNullOrEmpty(Materia.Predecesora))
                cadena = string.Format("insert into Materias values('{0}','{1}',null,'{3}')",
                Materia.Codigo, Materia.Materia, Materia.Antecesora,Materia.Predecesora);
            else if(!string.IsNullOrEmpty(Materia.Antecesora) && string.IsNullOrEmpty(Materia.Predecesora))
                cadena = string.Format("insert into Materias values('{0}','{1}','{2}',null)",
                Materia.Codigo, Materia.Materia, Materia.Antecesora, Materia.Predecesora);
            else if (string.IsNullOrEmpty(Materia.Antecesora) && string.IsNullOrEmpty(Materia.Predecesora))
                cadena = string.Format("insert into Materias values('{0}','{1}',null,null)",
                Materia.Codigo, Materia.Materia, Materia.Antecesora, Materia.Predecesora);
            else
                cadena = string.Format("insert into Materias values('{0}','{1}','{2}','{3}')",
                Materia.Codigo, Materia.Materia, Materia.Antecesora, Materia.Predecesora);
            _conexion.Ejecutar(cadena);
        }
        public void Actualizar(Materias Materia)
        {
            string cadena;
            if (string.IsNullOrEmpty(Materia.Antecesora) && !string.IsNullOrEmpty(Materia.Predecesora))
                cadena = string.Format("update Materias set Materia = '{1}',Antecesora = null,Predecesora ='{2}' where codigo = '{0}'",
                Materia.Codigo, Materia.Materia, Materia.Predecesora);
            else if (!string.IsNullOrEmpty(Materia.Antecesora) && string.IsNullOrEmpty(Materia.Predecesora))
                cadena = string.Format("update Materias set Materia = '{1}',Antecesora ='{2}',Predecesora = null where codigo = '{0}'",
                Materia.Codigo, Materia.Materia, Materia.Antecesora);
            else if (string.IsNullOrEmpty(Materia.Antecesora) && string.IsNullOrEmpty(Materia.Predecesora))
                cadena = string.Format("update Materias set Materia = '{1}',Antecesora = null,Predecesora = null where codigo = '{0}'",
                Materia.Codigo, Materia.Materia);
            else
                cadena = string.Format("update Materias set Materia = '{1}',Antecesora = '{2}',Predecesora ='{3}' where codigo = '{0}'",
                Materia.Codigo, Materia.Materia,Materia.Antecesora, Materia.Predecesora);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(string Codigo)
        {
            string cadena = string.Format("delete from Materias where Codigo = '{0}'", Codigo);
            _conexion.Ejecutar(cadena);
        }
        public List<Materias> Mostrar(string Codigo)
        {
            var list = new List<Materias>();
            string Consulta = string.Format("Select * from Materias where Codigo like '%{0}%'", Codigo);
            var ds = _conexion.ObtenerDatos(Consulta, "Materias");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Materia = new Materias()
                {
                    Codigo = row["Codigo"].ToString(),
                    Materia = row["Materia"].ToString(),
                    Predecesora = row["Predecesora"].ToString(),
                    Antecesora = row["Antecesora"].ToString()
                };
                list.Add(Materia);
            }
            return list;
        }
        public List<Materias> RegresaMaterias()
        {
            var list = new List<Materias>();
            string Consulta = "Select * from Materias";
            var ds = _conexion.ObtenerDatos(Consulta, "Materias");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Materia = new Materias()
                {
                    Codigo = row["Codigo"].ToString()
                };
                list.Add(Materia);
            }
            return list;
        }
    }
}
