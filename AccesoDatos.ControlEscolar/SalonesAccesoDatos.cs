﻿using Conexion;
using Entidades.ControlEscolar;
using System;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class SalonesAccesoDatos
    {
        private ConexionBD _conexion;
        public SalonesAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void GuardarActualizar(Salones Salon)
        {
            string cadena = "";
            if (Salon.Id == 0)
                cadena = string.Format("insert into Salones values(null,'{0}','{1}')", Salon.Grupo, Salon.Alumno);
            else
                cadena = string.Format("update salones set Grupo ='{1}',Alumno='{2}' where Id = {0}",
                Salon.Id, Salon.Grupo, Salon.Alumno);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(int ID)
        {
            string cadena = string.Format("delete from Salones where Id = {0}", ID);
            _conexion.Ejecutar(cadena);
        }
        public List<Salones> Mostrar(string Grupo)
        {
            var list = new List<Salones>();
            string Consulta = string.Format("Select * from Salones where Grupo like '%{0}%'", Grupo);
            var ds = _conexion.ObtenerDatos(Consulta, "Salones");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var salon = new Salones()
                {
                    Id = Convert.ToInt32(row["id"]),
                    Grupo = row["Grupo"].ToString(),
                    Alumno = row["Alumno"].ToString()
                };
                list.Add(salon);
            }
            return list;
        }
    }
}
