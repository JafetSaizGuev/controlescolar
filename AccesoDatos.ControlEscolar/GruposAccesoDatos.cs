﻿using Conexion;
using Entidades.ControlEscolar;
using System;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class GruposAccesoDatos
    {
        ConexionBD _conexion;
        public GruposAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void Guardar(Grupos Grupo)
        {
            string cadena = string.Format("insert into Grupos values('{0}','{1}',{2})",
            Grupo.ID,Grupo.Carrera,Grupo.AnioEntrada);
            _conexion.Ejecutar(cadena);
        }
        public void Actualizar(Grupos Grupo)
        {
            string cadena = string.Format("update Grupos set Carrera='{1}',AnioEntrada={2} where ID='{0}'",
            Grupo.ID, Grupo.Carrera, Grupo.AnioEntrada);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(string ID)
        {
            string cadena = string.Format("delete from Grupos where ID = '{0}'", ID);
            _conexion.Ejecutar(cadena);
        }
        public List<Grupos> Mostrar(string ID)
        {
            var list = new List<Grupos>();
            string Consulta = string.Format("Select * from Grupos where ID like '%{0}%'", ID);
            var ds = _conexion.ObtenerDatos(Consulta, "Grupos");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupos()
                {
                    ID = row["ID"].ToString(),
                    Carrera = row["Carrera"].ToString(),
                    AnioEntrada = Convert.ToInt32(row["AnioEntrada"])
                };
                list.Add(grupo);
            }
            return list;
        }
        public List<Grupos> RegresarGrupos()
        {
            var list = new List<Grupos>();
            string Consulta = "Select * from Grupos";
            var ds = _conexion.ObtenerDatos(Consulta, "Grupos");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupos()
                {
                    ID = row["ID"].ToString()
                };
                list.Add(grupo);
            }
            return list;
        }
    }
}
