﻿using Conexion;
using Entidades.ControlEscolar;
using System.Collections.Generic;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class CarrerasAccesoDatos
    {
        ConexionBD _conexion;
        public CarrerasAccesoDatos()
        {
            _conexion = new ConexionBD("localhost", "root", "", "VeryVal", 3306);
        }
        public void Guardar(Carreras Carrera)
        {
            string cadena = string.Format("insert into Carreras values('{0}','{1}','{2}')",
            Carrera.Clave,Carrera.Carrera,Carrera.Encargado);
            _conexion.Ejecutar(cadena);
        }
        public void Actualizar(Carreras Carrera)
        {
            string cadena = string.Format("update Carreras set Carrera='{1}',Encargado='{2}' where Clave='{0}'",
            Carrera.Clave, Carrera.Carrera, Carrera.Encargado);
            _conexion.Ejecutar(cadena);
        }
        public void Eliminar(string Clave)
        {
            string cadena = string.Format("delete from Carreras where Clave = '{0}'", Clave);
            _conexion.Ejecutar(cadena);
        }
        public List<Carreras> Mostrar(string Clave)
        {
            var list = new List<Carreras>();
            string Consulta = string.Format("Select * from Carreras where Clave like '%{0}%'", Clave);
            var ds = _conexion.ObtenerDatos(Consulta, "Carreras");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Carrera = new Carreras()
                {
                    Clave = row["Clave"].ToString(),
                    Carrera = row["Carrera"].ToString(),
                    Encargado = row["Encargado"].ToString()
                };
                list.Add(Carrera);
            }
            return list;
        }
        public List<Carreras> RegresarCarrera()
        {
            var list = new List<Carreras>();
            string Consulta = "Select * from Carreras";
            var ds = _conexion.ObtenerDatos(Consulta, "Carreras");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Carrera = new Carreras()
                {
                    Clave = row["Clave"].ToString()
                };
                list.Add(Carrera);
            }
            return list;
        }
    }
}
