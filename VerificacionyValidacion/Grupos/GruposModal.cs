﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Grupos
{
    public partial class frmGruposModal : Form
    {
        private GruposManejador _GruposManejador;
        private Entidades.ControlEscolar.Grupos _Grupos;
        private CarrerasManejador _CarrerasManejador;
        private bool Actualiza = false;
        public frmGruposModal()
        {
            InitializeComponent();
            _GruposManejador = new GruposManejador();
            _Grupos = new Entidades.ControlEscolar.Grupos();
            _CarrerasManejador = new CarrerasManejador();
        }
        public frmGruposModal(Entidades.ControlEscolar.Grupos Grupo)
        {
            InitializeComponent();
            _GruposManejador = new GruposManejador();
            _Grupos = new Entidades.ControlEscolar.Grupos();
            _CarrerasManejador = new CarrerasManejador();
            _Grupos = Grupo;
            Actualiza = true;
        }

        #region Acciones
        private void frmGruposModal_Load(object sender, EventArgs e)
        {
            LlenarAnio();
            LlenarCarrera();
            if (Actualiza)
                Rellenar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            if (!Actualiza)
                _GruposManejador.Guardar(_Grupos);
            else
                _GruposManejador.Actualizar(_Grupos);
            this.Close();
        }
        #endregion

        #region Metodos
        private void Llenar()
        {
            _Grupos.Carrera = cmbCarrera.Text;
            _Grupos.AnioEntrada = Convert.ToInt32(cmbAnio.Text);
            txtID.Text = cmbCarrera.Text + cmbAnio.Text;
            _Grupos.ID = txtID.Text;
        }
        private void LlenarAnio()
        {
            for (int i = 2000; i <= 2020; i++)
            {
                cmbAnio.Items.Add(i.ToString());
            }
        }
        private void Rellenar()
        {
            txtID.Text = _Grupos.ID;
            cmbCarrera.Text = _Grupos.Carrera;
            cmbAnio.Text = _Grupos.AnioEntrada.ToString();
        }
        private void LlenarCarrera()
        {
            var carreras = _CarrerasManejador.RegresarCarreras();
            foreach (var carrera in carreras)
            {
                cmbCarrera.Items.Add(carrera.Clave);
            }
        }
        #endregion
    }
}
