﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Grupos
{
    public partial class frmGrupos : Form
    {
        private GruposManejador _GruposManejador;
        private Entidades.ControlEscolar.Grupos _Grupos;
        public frmGrupos()
        {
            InitializeComponent();
            _GruposManejador = new GruposManejador();
            _Grupos = new Entidades.ControlEscolar.Grupos();
        }

        #region Acciones
        private void frmGrupos_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Grupos.frmGruposModal GM = new Grupos.frmGruposModal();
            GM.ShowDialog();
            Buscar("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void dgvGrupos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarGrupo();
            Grupos.frmGruposModal GM = new Grupos.frmGruposModal(_Grupos);
            GM.ShowDialog();
            Buscar("");
        }
        #endregion

        #region Metodos
        private void Buscar(string ID)
        {
            try
            {
                dgvGrupos.DataSource = _GruposManejador.Mostrar(ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            string id = dgvGrupos.CurrentRow.Cells["ID"].Value.ToString();
            _GruposManejador.Eliminar(id);
        }
        private void LlenarGrupo()
        {
            _Grupos.ID = dgvGrupos.CurrentRow.Cells["ID"].Value.ToString();
            _Grupos.Carrera = dgvGrupos.CurrentRow.Cells["Carrera"].Value.ToString();
            _Grupos.AnioEntrada = Convert.ToInt32(dgvGrupos.CurrentRow.Cells["AnioEntrada"].Value);
        }
        #endregion
    }
}
