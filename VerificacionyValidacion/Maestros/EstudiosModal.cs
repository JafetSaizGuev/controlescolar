﻿using Entidades.ControlEscolar;
using LogicaNegocios.ControlEscolar;
using Microsoft.VisualBasic.Devices;
using System;
using System.IO;
using System.Windows.Forms;
using Extencions.ControlEscolar;

namespace VerificacionyValidacion.Maestros
{
    public partial class frmEstudiosModal : Form
    {
        private string Maestro;
        private EstudiosManejador _EstudiosManejador;
        private Entidades.ControlEscolar.Estudios _Estudios;
        private RutasManager _RutasManager;
        private bool actualizar = false;
        public frmEstudiosModal(string Maestro)
        {
            InitializeComponent();
            this.Maestro = Maestro;
            _EstudiosManejador = new EstudiosManejador();
            _RutasManager = new RutasManager();
            _Estudios = new Estudios();
        }
        public frmEstudiosModal(Entidades.ControlEscolar.Estudios Estudios)
        {
            InitializeComponent();
            this._Estudios = Estudios;
            Maestro = Estudios.MaestroID;
            _EstudiosManejador = new EstudiosManejador();
            _RutasManager = new RutasManager();
            actualizar = true;
        }

        #region Acciones
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Guardar(txtArchivo.Text);
            this.Close();
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            var resultado = ofdArchivo.ShowDialog();
            if (resultado == DialogResult.OK)
                txtArchivo.Text = ofdArchivo.FileName;
        }

        private void FrmEstudiosModal_Load(object sender, EventArgs e)
        {
            if (actualizar)
                txtArchivo.Text = _Estudios.Archivo;
        }
        #endregion

        #region Metodos
        /*private bool GuardarEn(string Maestro,string Archivo)
        {
            Computer C = new Computer();
            try
            {
                if(!actualizar)
                {
                    string ruta = Path.Combine(Application.StartupPath, "Archivos", Maestro);
                    if (!Directory.Exists(ruta))
                        Directory.CreateDirectory(ruta);
                    ruta += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                    C.FileSystem.CopyFile(Archivo, ruta);
                }
                else
                {
                    string rutaD = Path.Combine(Application.StartupPath, "Archivos", _Estudios.MaestroID, _Estudios.Archivo);
                    C.FileSystem.DeleteFile(rutaD);
                    string rutaA = Path.Combine(Application.StartupPath, "Archivos", _Estudios.MaestroID);
                    rutaA += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                    C.FileSystem.CopyFile(Archivo, rutaA);
                    _Estudios.Archivo = Archivo.Substring(Archivo.LastIndexOf(@"\"));
                }
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
        }*/
        private void Guardar(string Archivo)
        {
            if (!actualizar)
            {
                _Estudios.MaestroID = Maestro;
                _Estudios.Archivo = Archivo.Substring(Archivo.LastIndexOf(@"\"));
            }
            if (_RutasManager.GuardarEstudio(actualizar,Application.StartupPath,Maestro, Archivo,_Estudios))
                _EstudiosManejador.GuardarActualizar(_Estudios);
        }
        #endregion
    }
}
