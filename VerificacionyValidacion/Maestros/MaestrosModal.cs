﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Maestros
{
    public partial class frmMaestrosModal : Form
    {
        private MaestrosManejador _MaestrosManejador;
        private EstadosManejador _EstadosManejador;
        private MunicipiosManejador _MunicipiosManejador;
        private Entidades.ControlEscolar.Maestros _Maestros;
        private bool Actualiza = false;
        public frmMaestrosModal()
        {
            InitializeComponent();
            _MaestrosManejador = new MaestrosManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _Maestros = new Entidades.ControlEscolar.Maestros();
        }
        public frmMaestrosModal(Entidades.ControlEscolar.Maestros Maestro)
        {
            InitializeComponent();
            _MaestrosManejador = new MaestrosManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _Maestros = new Entidades.ControlEscolar.Maestros();
            _Maestros = Maestro;
            Actualiza = true;
        }

        #region Acciones
        private void FrmMaestrosModal_Load(object sender, EventArgs e)
        {
            LLenarEstados();
            txtCodigo.Enabled = false;
            dtpNacimiento.Enabled = false;
            if (Actualiza)
            {
                btnDocumentos.Enabled = true;
                Rellenar();
                if (_Maestros.Sexo.Equals("Masculino"))
                    cmbSexo.SelectedIndex = 0;
                else
                    cmbSexo.SelectedIndex = 1;
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            try
            {
                if (!Actualiza)
                {
                    _MaestrosManejador.guardar(_Maestros);
                    Actualiza = true;
                    btnDocumentos.Enabled = true;
                }
                else
                    _MaestrosManejador.Actualizar(_Maestros);
                MessageBox.Show("Correcto");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void BtnDocumentos_Click(object sender, EventArgs e)
        {
            Llenar();
            if (_Maestros.Codigo != null)
            {
                Maestros.frmEstudios E = new Maestros.frmEstudios(_Maestros.Codigo);
                E.ShowDialog();
            }
        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            LLenarMunicipios(cmbEstado.SelectedIndex);
        }
        #endregion

        #region Metodos
        private void Llenar()
        {
            txtCodigo.Text = ClaveCompuesta(txtAPaterno.Text, txtAMaterno.Text, dtpNacimiento.Value.Year, dtpNacimiento.Value.Month);
            _Maestros.Nombre = txtNombre.Text;
            _Maestros.APaterno = txtAPaterno.Text;
            _Maestros.AMaterno = txtAMaterno.Text;
            _Maestros.Sexo = cmbSexo.Text;
            _Maestros.Nacimiento = dtpNacimiento.Value.ToString();
            _Maestros.Correo = txtCorreo.Text;
            _Maestros.Estado = cmbEstado.Text;
            _Maestros.Municipio = cmbMunicipio.Text;
            _Maestros.Bancaria = txtBancaria.Text;
            _Maestros.Codigo = txtCodigo.Text;
        }
        private void LLenarEstados()
        {
            var Estados = _EstadosManejador.RegresarEstados();
            foreach (var estado in Estados)
            {
                cmbEstado.Items.Add(estado.Estado);
            }
        }
        private void LLenarMunicipios(int estado)
        {
            cmbMunicipio.Items.Clear();
            int filtro = cmbEstado.SelectedIndex;
            var Municipios = _MunicipiosManejador.RegresarMunicipios(filtro + 1);
            foreach (var municipio in Municipios)
            {
                cmbMunicipio.Items.Add(municipio.Municipio);
            }
        }
        private void Rellenar()
        {
            txtCodigo.Text = _Maestros.Codigo;
            txtNombre.Text = _Maestros.Nombre;
            txtAPaterno.Text = _Maestros.APaterno;
            txtAMaterno.Text = _Maestros.AMaterno;
            cmbSexo.Text = _Maestros.Sexo;
            dtpNacimiento.Text = _Maestros.Nacimiento;
            txtCorreo.Text = _Maestros.Correo;
            cmbEstado.Text = _Maestros.Estado;
            cmbMunicipio.Text = _Maestros.Municipio;
            txtBancaria.Text = _Maestros.Bancaria;
        }
        private string ClaveCompuesta(string D1, string D2, int anio,int mes)
        {
            string Codigo = D1.Substring(0, 1) + D2.Substring(0, 1) + anio.ToString() + mes.ToString();
            return Codigo;
        }
        #endregion
    }
}
