﻿using LogicaNegocios.ControlEscolar;
using Microsoft.VisualBasic.Devices;
using System;
using System.Windows.Forms;
using Extencions.ControlEscolar;

namespace VerificacionyValidacion.Maestros
{
    public partial class frmEstudios : Form
    {
        private EstudiosManejador _EstudiosManejador;
        private Entidades.ControlEscolar.Estudios _Estudios;
        private RutasManager _RutasManager;
        private string Maestro = "";
        
        public frmEstudios(string MaestroCodigo)
        {
            InitializeComponent();
            _EstudiosManejador = new EstudiosManejador();
            _Estudios = new Entidades.ControlEscolar.Estudios();
            _RutasManager = new RutasManager();
            this.Maestro = MaestroCodigo;
            this.Text = "Estudios de " + MaestroCodigo;
        }
        #region Acciones
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            frmEstudiosModal EM = new frmEstudiosModal(Maestro);
            EM.ShowDialog();
            Buscar(Maestro);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar(Maestro);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void FrmEstudios_Load(object sender, EventArgs e)
        {
            Buscar(Maestro);
        }

        private void DgvEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarEstudio();
            frmEstudiosModal EM = new frmEstudiosModal(_Estudios);
            EM.ShowDialog();
            Buscar(Maestro);
        }
        #endregion

        #region Metodos
        private void Buscar(string Codigo)
        {
            try
            {
                dgvEstudios.DataSource = _EstudiosManejador.Mostrar(Codigo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            int id = Convert.ToInt32(dgvEstudios.CurrentRow.Cells["id"].Value);
            _EstudiosManejador.Eliminar(id);
            _RutasManager.EliminarEstudio(dgvEstudios);
        }
        private void LlenarEstudio()
        {
            _Estudios.Id = Convert.ToInt32(dgvEstudios.CurrentRow.Cells["id"].Value);
            _Estudios.MaestroID = dgvEstudios.CurrentRow.Cells["MaestroID"].Value.ToString();
            _Estudios.Archivo = dgvEstudios.CurrentRow.Cells["Archivo"].Value.ToString();
        }
        #endregion
    }
}
