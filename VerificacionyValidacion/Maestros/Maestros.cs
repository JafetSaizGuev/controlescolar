﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Maestros
{
    public partial class frmMaestros : Form
    {
        private MaestrosManejador _MaestrosManejador;
        private Entidades.ControlEscolar.Maestros _Maestros;

        public frmMaestros()
        {
            InitializeComponent();
            _MaestrosManejador = new MaestrosManejador();
            _Maestros = new Entidades.ControlEscolar.Maestros();
        }

        #region Acciones
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            Maestros.frmMaestrosModal MM = new Maestros.frmMaestrosModal();
            MM.ShowDialog();
            Buscar("");
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void FrmMaestros_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void DgvMaestros_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LLenarUsuario();
            Maestros.frmMaestrosModal MM = new Maestros.frmMaestrosModal(_Maestros);
            MM.ShowDialog();
            Buscar("");
        }

        #endregion

        #region Metodos
        private void Buscar(string Codigo)
        {
            try
            {
                dgvMaestros.DataSource = _MaestrosManejador.Mostrar(Codigo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            string codigo = dgvMaestros.CurrentRow.Cells["Codigo"].Value.ToString();
            _MaestrosManejador.Eliminar(codigo);
        }
        private void LLenarUsuario()
        {
            _Maestros.Codigo = dgvMaestros.CurrentRow.Cells["Codigo"].Value.ToString();
            _Maestros.Nombre = dgvMaestros.CurrentRow.Cells["Nombre"].Value.ToString();
            _Maestros.APaterno = dgvMaestros.CurrentRow.Cells["APaterno"].Value.ToString();
            _Maestros.AMaterno = dgvMaestros.CurrentRow.Cells["AMaterno"].Value.ToString();
            _Maestros.Sexo = dgvMaestros.CurrentRow.Cells["Sexo"].Value.ToString();
            _Maestros.Nacimiento = dgvMaestros.CurrentRow.Cells["Nacimiento"].Value.ToString();
            _Maestros.Correo = dgvMaestros.CurrentRow.Cells["Correo"].Value.ToString();
            _Maestros.Estado = dgvMaestros.CurrentRow.Cells["Estado"].Value.ToString();
            _Maestros.Municipio = dgvMaestros.CurrentRow.Cells["Municipio"].Value.ToString();
            _Maestros.Bancaria = dgvMaestros.CurrentRow.Cells["Bancaria"].Value.ToString();
        }
        #endregion
    }
}
