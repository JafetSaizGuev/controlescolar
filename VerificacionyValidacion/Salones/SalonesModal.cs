﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Salones
{
    public partial class frmSalonesModal : Form
    {
        private SalonesManejador _SalonesManejador;
        private AlumnosManejador _AlumnosManejador;
        private GruposManejador _GruposManejador;
        private Entidades.ControlEscolar.Salones _Salones;
        private bool Actualiza = false;
        public frmSalonesModal()
        {
            InitializeComponent();
            _SalonesManejador = new SalonesManejador();
            _Salones = new Entidades.ControlEscolar.Salones();
            _AlumnosManejador = new AlumnosManejador();
            _GruposManejador = new GruposManejador();
        }
        public frmSalonesModal(Entidades.ControlEscolar.Salones Salon)
        {
            InitializeComponent();
            _SalonesManejador = new SalonesManejador();
            _Salones = new Entidades.ControlEscolar.Salones();
            _AlumnosManejador = new AlumnosManejador();
            _GruposManejador = new GruposManejador();
            _Salones = Salon;
            Actualiza = true;
        }
        #region Accion
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            _SalonesManejador.GuardarActualizar(_Salones);
            this.Close();
        }

        private void frmSalonesModal_Load(object sender, EventArgs e)
        {
            LlenarAlumno();
            LlenarGrupo();
            if (Actualiza)
                Rellenar();
        }
        #endregion

        #region Metodos
        private void Llenar()
        {
            if (!string.IsNullOrEmpty(txtId.Text))
                _Salones.Id = Convert.ToInt32(txtId.Text);
            _Salones.Grupo = cmbGrupo.Text;
            _Salones.Alumno = cmbAlumno.Text;
        }
        private void Rellenar()
        {
            txtId.Text = _Salones.Id.ToString();
            cmbGrupo.Text = _Salones.Grupo;
            cmbAlumno.Text = _Salones.Alumno;
        }
        private void LlenarGrupo()
        {
            var grupos = _GruposManejador.RegresarGrupos();
            foreach (var grupo in grupos)
            {
                cmbGrupo.Items.Add(grupo.ID);
            }
        }
        private void LlenarAlumno()
        {
            var alumnos = _AlumnosManejador.RegresarAlumnos();
            foreach (var alumno in alumnos)
            {
                cmbAlumno.Items.Add(alumno.NoControl);
            }
        }
        #endregion
    }
}
