﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Salones
{
    public partial class frmSalones : Form
    {
        private Entidades.ControlEscolar.Salones _Salones;
        private SalonesManejador _SalonesManejador;
        public frmSalones()
        {
            InitializeComponent();
            _SalonesManejador = new SalonesManejador();
            _Salones = new Entidades.ControlEscolar.Salones();
        }
        #region Acciones
        private void frmSalones_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmSalonesModal SM = new frmSalonesModal();
            SM.ShowDialog();
            Buscar("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void dgvSalones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarSalon();
            frmSalonesModal SM = new frmSalonesModal(_Salones);
            SM.ShowDialog();
            Buscar("");
        }
        #endregion

        #region Metodos
        private void Buscar(string Grupo)
        {
            try
            {
                dgvSalones.DataSource = _SalonesManejador.Mostrar(Grupo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            int clave = Convert.ToInt32(dgvSalones.CurrentRow.Cells["id"].Value);
            _SalonesManejador.Eliminar(clave);
        }
        private void LlenarSalon()
        {
            _Salones.Id = Convert.ToInt32(dgvSalones.CurrentRow.Cells["id"].Value);
            _Salones.Grupo = dgvSalones.CurrentRow.Cells["Grupo"].Value.ToString();
            _Salones.Alumno = dgvSalones.CurrentRow.Cells["Alumno"].Value.ToString();
        }
        #endregion
    }
}
