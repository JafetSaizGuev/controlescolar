﻿using LogicaNegocios.ControlEscolar;
using Microsoft.VisualBasic.Devices;
using System;
using System.IO;
using System.Windows.Forms;
using Extencions.ControlEscolar;

namespace VerificacionyValidacion.Escuelas
{
    public partial class frmEscuelas : Form
    {
        private EscuelasManejador _EscuelasManejador;
        private RutasManager _RutasManager;
        private Entidades.ControlEscolar.Escuelas _Escuelas;
        public frmEscuelas()
        {
            InitializeComponent();
            _EscuelasManejador = new EscuelasManejador();
            _RutasManager = new RutasManager();
            _Escuelas = new Entidades.ControlEscolar.Escuelas();
        } 

        #region Acciones
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            frmEscuelasModal EM = new frmEscuelasModal();
            EM.ShowDialog();
            Buscar("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void dgvEscuelas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarEscuela();
            frmEscuelasModal EM = new frmEscuelasModal(_Escuelas,ptbLogo.ImageLocation);
            EM.ShowDialog();
            Buscar("");
        }

        private void dgvEscuelas_SelectionChanged(object sender, EventArgs e)
        {
            _RutasManager.MostrarLogo(ptbLogo,Application.StartupPath,dgvEscuelas);
        }

        private void frmEscuelas_Load(object sender, EventArgs e)
        {
            Buscar("");
        }
        #endregion

        #region Metodos
        private void Buscar(string Escuela)
        {
            try
            {
                dgvEscuelas.DataSource = _EscuelasManejador.Mostrar(Escuela);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            int id = Convert.ToInt32(dgvEscuelas.CurrentRow.Cells["id"].Value);
            _RutasManager.EliminarRutaEscuela(Application.StartupPath,dgvEscuelas);
            _EscuelasManejador.Eliminar(id);
        }
        private void LlenarEscuela()
        {
            _Escuelas.ID = Convert.ToInt32(dgvEscuelas.CurrentRow.Cells["id"].Value);
            _Escuelas.Nombre = dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString();
            _Escuelas.Domicilio = dgvEscuelas.CurrentRow.Cells["Domicilio"].Value.ToString();
            _Escuelas.Estado = dgvEscuelas.CurrentRow.Cells["Estado"].Value.ToString();
            _Escuelas.Municipio = dgvEscuelas.CurrentRow.Cells["Municipio"].Value.ToString();
            _Escuelas.NumeroExterior = dgvEscuelas.CurrentRow.Cells["NumeroExterior"].Value.ToString();
            _Escuelas.Telefono = dgvEscuelas.CurrentRow.Cells["Telefono"].Value.ToString();
            _Escuelas.PaginaWeb = dgvEscuelas.CurrentRow.Cells["PaginaWeb"].Value.ToString();
            _Escuelas.DirectorActual = dgvEscuelas.CurrentRow.Cells["DirectorActual"].Value.ToString();
            _Escuelas.Logo = dgvEscuelas.CurrentRow.Cells["Logo"].Value.ToString();
        }
        /*private void MostrarLogo()
        {
            ptbLogo.ImageLocation = Path.Combine(Application.StartupPath, "logos", dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString(), dgvEscuelas.CurrentRow.Cells["Logo"].Value.ToString());
        }
        private void EliminarRuta()
        {
            Computer C = new Computer();
            if(C.FileSystem.DirectoryExists(Path.Combine(Application.StartupPath, "logos", dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString())))
                C.FileSystem.DeleteDirectory(Path.Combine(Application.StartupPath, "logos", dgvEscuelas.CurrentRow.Cells["Nombre"].Value.ToString()),Microsoft.VisualBasic.FileIO.DeleteDirectoryOption.DeleteAllContents);
        }*/
        #endregion
    }
}
