﻿using System;
using Microsoft.VisualBasic.Devices;
using System.Windows.Forms;
using LogicaNegocios.ControlEscolar;
using System.IO;
using Extencions.ControlEscolar;

namespace VerificacionyValidacion.Escuelas
{
    public partial class frmEscuelasModal : Form
    {
        private EscuelasManejador _EscuelasManejador;
        private EstadosManejador _EstadosManejador;
        private MunicipiosManejador _MunicipiosManejador;
        private RutasManager _RutasManager;
        private Entidades.ControlEscolar.Escuelas _Escuelas;
        private bool Actualizar = false;
        private string ArchivoViejo = "";

        public frmEscuelasModal()
        {
            InitializeComponent();
            _EscuelasManejador = new EscuelasManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _RutasManager = new RutasManager();
            _Escuelas = new Entidades.ControlEscolar.Escuelas();
        }
        public frmEscuelasModal(Entidades.ControlEscolar.Escuelas Escuela,string logo)
        {
            InitializeComponent();
            _EscuelasManejador = new EscuelasManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _RutasManager = new RutasManager();
            _Escuelas = new Entidades.ControlEscolar.Escuelas();
            _Escuelas = Escuela;
            ArchivoViejo = logo;
            Actualizar = true;
        }

        #region Acciones
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            if(_RutasManager.GuardarEscuelas(Actualizar,Application.StartupPath,ptbLogo.ImageLocation,_Escuelas,ArchivoViejo))
                _EscuelasManejador.Guardar(_Escuelas);
            this.Close();
        }

        private void btnBuscarImagen_Click(object sender, EventArgs e)
        {
            var resultado = ofdLogo.ShowDialog();
            if (resultado == DialogResult.OK)
                ptbLogo.ImageLocation = ofdLogo.FileName;
        }

        private void cmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            LLenarMunicipios(cmbEstado.SelectedIndex);
        }

        private void frmEscuelasModal_Load(object sender, EventArgs e)
        {
            LLenarEstados();
            if (Actualizar)
                Rellenar();
        }
        #endregion

        #region Metodos
        private void LLenarEstados()
        {
            var Estados = _EstadosManejador.RegresarEstados();
            foreach (var estado in Estados)
            {
                cmbEstado.Items.Add(estado.Estado);
            }
        }
        private void LLenarMunicipios(int estado)
        {
            cmbMunicipio.Items.Clear();
            int filtro = cmbEstado.SelectedIndex;
            var Municipios = _MunicipiosManejador.RegresarMunicipios(filtro + 1);
            foreach (var municipio in Municipios)
            {
                cmbMunicipio.Items.Add(municipio.Municipio);
            }
        }
        private void Llenar()
        {
            if(txtControl.Text != "")
                _Escuelas.ID = Convert.ToInt32(txtControl.Text);
            _Escuelas.Nombre = txtNombre.Text;
            _Escuelas.DirectorActual = txtDirector.Text;
            _Escuelas.NumeroExterior = txtNumeroExterior.Text;
            _Escuelas.PaginaWeb = txtPaginaWeb.Text;
            _Escuelas.Telefono = txtTelefono.Text;
            _Escuelas.Estado = cmbEstado.Text;
            _Escuelas.Municipio = cmbMunicipio.Text;
            _Escuelas.Domicilio = txtDomicilio.Text;
        }
        private void Rellenar()
        {
            txtControl.Text = _Escuelas.ID.ToString();
            txtNombre.Text = _Escuelas.Nombre;
            txtDirector.Text = _Escuelas.DirectorActual;
            txtNumeroExterior.Text = _Escuelas.NumeroExterior;
            txtPaginaWeb.Text = _Escuelas.PaginaWeb;
            txtTelefono.Text = _Escuelas.Telefono;
            cmbEstado.Text = _Escuelas.Estado;
            cmbMunicipio.Text = _Escuelas.Municipio;
            txtDomicilio.Text = _Escuelas.Domicilio;
            ptbLogo.ImageLocation = ArchivoViejo;
        }
        /*private bool GuardarEn(string Escuela, string Archivo)
        {
            Computer C = new Computer();
            var ArchivoInfo = new FileInfo(Archivo);
            if (ArchivoInfo.Length < 10000)
            {
                try
                {
                    if (!Actualizar)
                    {
                        string ruta = Path.Combine(Application.StartupPath, "logos", Escuela);
                        if (!Directory.Exists(ruta))
                            Directory.CreateDirectory(ruta);
                        ruta += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                        C.FileSystem.CopyFile(Archivo, ruta);
                    }
                    else
                    {
                        C.FileSystem.DeleteFile(ArchivoViejo);
                        string rutaA = Path.Combine(Application.StartupPath, "logos", Escuela);
                        rutaA += Archivo.Substring(Archivo.LastIndexOf(@"\"));
                        C.FileSystem.CopyFile(Archivo, rutaA);
                    }
                    _Escuelas.Logo = Archivo.Substring(Archivo.LastIndexOf(@"\"));
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error de ruta", MessageBoxButtons.OK);
                    return false;
                }
            }
            else
            {
                MessageBox.Show("El tamaño de la imagen es mayor a 10 kilobit", "Error de tamaño en imagen", MessageBoxButtons.OK);
                return false;
            }
        }*/
        #endregion
    }
}
