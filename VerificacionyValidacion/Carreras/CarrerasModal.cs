﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Carreras
{
    public partial class frmCarrerasModal : Form
    {
        private CarrerasManejador _CarrerasManejador;
        private MaestrosManejador _MaestrosManejador;
        private Entidades.ControlEscolar.Carreras _Carreras;
        private bool actualiza = false;
        public frmCarrerasModal()
        {
            InitializeComponent();
            _Carreras = new Entidades.ControlEscolar.Carreras();
            _CarrerasManejador = new CarrerasManejador();
            _MaestrosManejador = new MaestrosManejador();
        }
        public frmCarrerasModal(Entidades.ControlEscolar.Carreras Carrera)
        {
            InitializeComponent();
            _Carreras = new Entidades.ControlEscolar.Carreras();
            _CarrerasManejador = new CarrerasManejador();
            _MaestrosManejador = new MaestrosManejador();
            _Carreras = Carrera;
            actualiza = true;
            txtCarrera.Enabled = false;
        }
        #region Acciones
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            if (!actualiza)
                _CarrerasManejador.Guardar(_Carreras);
            else
                _CarrerasManejador.Actualizar(_Carreras);
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCarrerasModal_Load(object sender, EventArgs e)
        {
            LlenarMaestros();
            if(actualiza)
                Rellenar();
        }
        #endregion

        #region Metodos
        private void LlenarMaestros()
        {
            var Maestros = _MaestrosManejador.RegresarMaestros("");
            foreach (var maestro in Maestros)
            {
                cmbEncargado.Items.Add(maestro.Nombre);
            }
        }
        private void Llenar()
        {
            _Carreras.Carrera = txtCarrera.Text;
            _Carreras.Encargado = cmbEncargado.Text;
            string temp = "";
            if (txtCarrera.Text.LastIndexOf(@" ") > 0)
                temp = txtCarrera.Text.Substring(0, txtCarrera.Text.LastIndexOf(@" "));
            else
                temp = txtCarrera.Text.Substring(0, 3);
            _Carreras.Clave = temp;
        }
        private void Rellenar()
        {
            txtClave.Text = _Carreras.Clave;
            txtCarrera.Text = _Carreras.Carrera;
            cmbEncargado.Text = _Carreras.Encargado;
        }
        #endregion
    }
}
