﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Carreras
{
    public partial class frmCarreras : Form
    {
        private CarrerasManejador _CarrerasManejador;
        private Entidades.ControlEscolar.Carreras _Carreras;

        public frmCarreras()
        {
            InitializeComponent();
            _CarrerasManejador = new CarrerasManejador();
            _Carreras = new Entidades.ControlEscolar.Carreras();
        }

        #region Acciones
        private void frmCarreras_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Carreras.frmCarrerasModal CM = new Carreras.frmCarrerasModal();
            CM.ShowDialog();
            Buscar("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void dgvCarreras_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarCarrera();
            Carreras.frmCarrerasModal CM = new Carreras.frmCarrerasModal();
            CM.ShowDialog();
            Buscar("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
        #endregion

        #region Metodos
        private void Buscar(string Clave)
        {
            try
            {
                dgvCarreras.DataSource = _CarrerasManejador.Mostrar(Clave);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            string Clave = dgvCarreras.CurrentRow.Cells["Clave"].Value.ToString();
            _CarrerasManejador.Eliminar(Clave);
        }
        private void LlenarCarrera()
        { 
            _Carreras.Clave = dgvCarreras.CurrentRow.Cells["Clave"].Value.ToString();
            _Carreras.Carrera = dgvCarreras.CurrentRow.Cells["Carrera"].Value.ToString();
            _Carreras.Encargado = dgvCarreras.CurrentRow.Cells["Encargado"].Value.ToString();
        }
        #endregion
    }
}
