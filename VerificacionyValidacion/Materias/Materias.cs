﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Materias
{
    public partial class frmMaterias : Form
    {
        private MateriasManejador _MateriasManejador;
        private Entidades.ControlEscolar.Materias _Materias;
        public frmMaterias()
        {
            InitializeComponent();
            _MateriasManejador = new MateriasManejador();
            _Materias = new Entidades.ControlEscolar.Materias();
        }

        #region Acciones
        private void frmMaterias_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Materias.frmMateriasModal MM = new Materias.frmMateriasModal();
            MM.ShowDialog();
            Buscar("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void dgvMaterias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarMateria();
            Materias.frmMateriasModal MM = new Materias.frmMateriasModal(_Materias);
            MM.ShowDialog();
            Buscar("");
        }
        #endregion

        #region Metodos
        private void Buscar(string Codigo)
        {
            try
            {
                dgvMaterias.DataSource = _MateriasManejador.Mostrar(Codigo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            string clave = dgvMaterias.CurrentRow.Cells["Codigo"].Value.ToString();
            _MateriasManejador.Eliminar(clave);
        }
        private void LlenarMateria()
        {
            _Materias.Codigo = dgvMaterias.CurrentRow.Cells["Codigo"].Value.ToString();
            _Materias.Materia = dgvMaterias.CurrentRow.Cells["Materia"].Value.ToString();
            _Materias.Antecesora = dgvMaterias.CurrentRow.Cells["Antecesora"].Value.ToString();
            _Materias.Predecesora = dgvMaterias.CurrentRow.Cells["Predecesora"].Value.ToString();
        }
        #endregion
    }
}
