﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Materias
{
    public partial class frmMateriasModal : Form
    {
        private MateriasManejador _MateriasManejador;
        private Entidades.ControlEscolar.Materias _Materias;
        private bool Actualiza = false;
        public frmMateriasModal()
        {
            InitializeComponent();
            _MateriasManejador = new MateriasManejador();
            _Materias = new Entidades.ControlEscolar.Materias();
        }
        public frmMateriasModal(Entidades.ControlEscolar.Materias Materia)
        {
            InitializeComponent();
            _MateriasManejador = new MateriasManejador();
            _Materias = new Entidades.ControlEscolar.Materias();
            _Materias = Materia;
            Actualiza = true;
        }

        #region Acciones
        private void frmMateriasModal_Load(object sender, EventArgs e)
        {
            LlenarMateria(cmbAntecesora);
            LlenarMateria(cmbPredecesora);
            if (Actualiza)
                Rellenar();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            if (!Actualiza)
                _MateriasManejador.Guardar(_Materias);
            else
                _MateriasManejador.Actualizar(_Materias);
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Metodos
        private void LlenarMateria(ComboBox comboBox)
        {
            var Materias = _MateriasManejador.RegresarMaterias();
            comboBox.Items.Add("");
            foreach (var materia in Materias)
            {
                comboBox.Items.Add(materia.Codigo);
            }
        }
        private void Llenar()
        {
            _Materias.Materia = txtMateria.Text;
            _Materias.Antecesora = cmbAntecesora.Text;
            _Materias.Predecesora = cmbPredecesora.Text;
            txtCodigo.Text = txtMateria.Text.Substring(0, 4);
            _Materias.Codigo = txtCodigo.Text;
        }
        private void Rellenar()
        {
            txtCodigo.Text = _Materias.Codigo;
            txtMateria.Text = _Materias.Materia;
            cmbAntecesora.Text = _Materias.Antecesora;
            cmbPredecesora.Text = _Materias.Predecesora;
        }
        #endregion
    }
}
