﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.CargasAcademicas
{
    public partial class frmCargasAcademicasModal : Form
    {
        private CargasAcademicasManejador _CargasAcademicasManejador;
        private MateriasManejador _MateriasManejador;
        private GruposManejador _GruposManejador;
        private Entidades.ControlEscolar.CargasAcademicas _CargasAcademicas;
        private bool Actualiza = false;
        public frmCargasAcademicasModal()
        {
            InitializeComponent();
            _CargasAcademicasManejador = new CargasAcademicasManejador();
            _CargasAcademicas = new Entidades.ControlEscolar.CargasAcademicas();
            _MateriasManejador = new MateriasManejador();
            _GruposManejador = new GruposManejador();
        }
        public frmCargasAcademicasModal(Entidades.ControlEscolar.CargasAcademicas cargaAcademica)
        {
            InitializeComponent();
            _CargasAcademicasManejador = new CargasAcademicasManejador();
            _CargasAcademicas = new Entidades.ControlEscolar.CargasAcademicas();
            _MateriasManejador = new MateriasManejador();
            _GruposManejador = new GruposManejador();
            _CargasAcademicas = cargaAcademica;
            Actualiza = true;
        }
        #region Acciones
        private void frmCargasAcademicasModal_Load(object sender, EventArgs e)
        {
            LlenarGrupo();
            LlenarMateria();
            if (Actualiza)
                Rellenar();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Llenar();
            _CargasAcademicasManejador.GuardarActualizar(_CargasAcademicas);
            this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Metodos
        private void Llenar()
        {
            if(!string.IsNullOrEmpty(txtID.Text))
                _CargasAcademicas.Id = Convert.ToInt32(txtID.Text);
            _CargasAcademicas.Grupo = cmbGrupo.Text;
            _CargasAcademicas.Materia = cmbMateria.Text;
        }
        private void Rellenar()
        {
            txtID.Text = _CargasAcademicas.Id.ToString();
            cmbGrupo.Text = _CargasAcademicas.Grupo;
            cmbMateria.Text = _CargasAcademicas.Materia;
        }
        private void LlenarGrupo()
        {
            var grupos = _GruposManejador.RegresarGrupos();
            foreach (var grupo in grupos)
            {
                cmbGrupo.Items.Add(grupo.ID);
            }
        }
        private void LlenarMateria()
        {
            var materias = _MateriasManejador.RegresarMaterias();
            foreach (var materia in materias)
            {
                cmbMateria.Items.Add(materia.Codigo);
            }
        }
        #endregion
    }
}
