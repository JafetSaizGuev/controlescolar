﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.CargasAcademicas
{
    public partial class frmCargasAcademicas : Form
    {
        private CargasAcademicasManejador _CargasAcademicasManejador;
        private Entidades.ControlEscolar.CargasAcademicas _CargasAcademicas;
        public frmCargasAcademicas()
        {
            InitializeComponent();
            _CargasAcademicasManejador = new CargasAcademicasManejador();
            _CargasAcademicas = new Entidades.ControlEscolar.CargasAcademicas();
        }
        #region Acciones
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            CargasAcademicas.frmCargasAcademicasModal CAM = new frmCargasAcademicasModal();
            CAM.ShowDialog();
            Buscar("");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void dgvCargasAcademicas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LlenarCarga();
            frmCargasAcademicasModal CAM = new frmCargasAcademicasModal(_CargasAcademicas);
            CAM.ShowDialog();
            Buscar("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
        private void frmCargasAcademicas_Load(object sender, EventArgs e)
        {
            Buscar("");
        }
    #endregion

    #region Metodos
    private void Buscar(string Grupo)
        {
            try
            {
                dgvCargasAcademicas.DataSource = _CargasAcademicasManejador.Mostrar(Grupo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            int id = Convert.ToInt32(dgvCargasAcademicas.CurrentRow.Cells["id"].Value);
            _CargasAcademicasManejador.Eliminar(id);
        }
        private void LlenarCarga()
        {
            _CargasAcademicas.Id = Convert.ToInt32(dgvCargasAcademicas.CurrentRow.Cells["id"].Value);
            _CargasAcademicas.Grupo = dgvCargasAcademicas.CurrentRow.Cells["Grupo"].Value.ToString();
            _CargasAcademicas.Materia = dgvCargasAcademicas.CurrentRow.Cells["Materia"].Value.ToString();
        }
        #endregion
    }
}
