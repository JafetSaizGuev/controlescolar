﻿using Extencions.ControlEscolar;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace VerificacionyValidacion
{
    public partial class frmRespaldo : Form
    {
        private string Respaldo = "";
        private RespaldosManager _RespaldosManager;

        public frmRespaldo()
        {
            InitializeComponent();
            _RespaldosManager = new RespaldosManager();
        }

        #region Acciones
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (!bwWork.IsBusy)
                bwWork.RunWorkerAsync();
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            var resultado = ofdRespaldo.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                txtDestino.Text = ofdRespaldo.FileName;
                Respaldo = "Restaurar";
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var resultado = fbdCarpeta.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                txtDestino.Text = fbdCarpeta.SelectedPath;
                Respaldo = "Guardar";
            }
        }
        #endregion
        #region Metodos

        /*private async void GenerarRespaldo(string Ruta)
        {
            try
            {
                var fecha = new DateTime();
                fecha = DateTime.Now;
                string nombreRespaldo = string.Format("ControlEscolar-{0}.zip", fecha.ToString("dd-MM-yyyy_HH_mm_ss"));
                string _logos = Path.Combine(Application.StartupPath, "logos");
                string _Archivos = Path.Combine(Application.StartupPath, "Archivos");
                string _Ruta = Path.Combine(Ruta, nombreRespaldo);
                DirectoryInfo dirLogo = new DirectoryInfo(_logos);
                DirectoryInfo dirArchivo = new DirectoryInfo(_Archivos);
                using (ZipFile zip = new ZipFile())
                {
                    if (dirLogo.Exists)
                    {
                        zip.AddDirectoryByName("logos");
                        zip.AddDirectory(_logos, "logos");
                    }
                    if (dirArchivo.Exists)
                    {
                        zip.AddDirectoryByName("Archivos");
                        zip.AddDirectory(_Archivos, "Archivos");
                    }
                    zip.Save(_Ruta);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private async void RestaurarRespaldo(string Respaldo)
        {
            using (ZipFile zip = ZipFile.Read(Respaldo))
            {
                zip.ExtractAll(Application.StartupPath);
                zip.Dispose();
            }
        }*/
        private void bwWork_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (Respaldo)
            {
                case "Restaurar":
                    _RespaldosManager.RestaurarRespaldo(Application.StartupPath,txtDestino.Text);
                    break;
                case "Guardar":
                    _RespaldosManager.GenerarRespaldo(Application.StartupPath,txtDestino.Text);
                    break;
                default:
                    txtDestino.Text = "No seas mamon";
                    break;
            }
        }
        private void bwWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                MessageBox.Show("Respaldo Cancelado");
            else if (e.Error != null)
                MessageBox.Show(e.Error.Message);
            else
            {
                MessageBox.Show("Respaldo Exitoso");
            }
        }
        #endregion
    }
}
