﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion
{
    public partial class frmAlumnos : Form
    {
        private AlumnosManejador _AlumnosManejador;
        private Entidades.ControlEscolar.Alumnos _Alumnos;

        public frmAlumnos()
        {
            InitializeComponent();
            _AlumnosManejador = new AlumnosManejador();
            _Alumnos = new Entidades.ControlEscolar.Alumnos();
        }

        #region Acciones
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }

        private void FrmAlumnos_Load(object sender, EventArgs e)
        {
            Buscar("");
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de Eliminar Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }
            }
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            Alumnos.frmAlumnosModal AM = new Alumnos.frmAlumnosModal();
            AM.ShowDialog();
            Buscar("");
        }

        private void DgvAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LLenarUsuario();
            Alumnos.frmAlumnosModal AM = new Alumnos.frmAlumnosModal(_Alumnos);
            AM.ShowDialog();
            Buscar("");
        }
        #endregion

        #region Metodos
        private void Buscar(string NoControl)
        {
            try
            {
                dgvAlumnos.DataSource = _AlumnosManejador.Mostrar(NoControl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Eliminar()
        {
            int id = Convert.ToInt32(dgvAlumnos.CurrentRow.Cells["NoControl"].Value);
            _AlumnosManejador.Eliminar(id);
        }
        private void LLenarUsuario()
        {
            _Alumnos.NoControl = dgvAlumnos.CurrentRow.Cells["NoControl"].Value.ToString();
            _Alumnos.Nombre = dgvAlumnos.CurrentRow.Cells["Nombre"].Value.ToString();
            _Alumnos.APaterno = dgvAlumnos.CurrentRow.Cells["APaterno"].Value.ToString();
            _Alumnos.AMaterno = dgvAlumnos.CurrentRow.Cells["AMaterno"].Value.ToString();
            _Alumnos.Sexo = dgvAlumnos.CurrentRow.Cells["Sexo"].Value.ToString();
            _Alumnos.Nacimiento = dgvAlumnos.CurrentRow.Cells["Nacimiento"].Value.ToString();
            _Alumnos.Correo = dgvAlumnos.CurrentRow.Cells["Correo"].Value.ToString();
            _Alumnos.Telefono = dgvAlumnos.CurrentRow.Cells["Telefono"].Value.ToString();
            _Alumnos.Estado = dgvAlumnos.CurrentRow.Cells["Estado"].Value.ToString();
            _Alumnos.Municipio = dgvAlumnos.CurrentRow.Cells["Municipio"].Value.ToString();
            _Alumnos.Domicilio = dgvAlumnos.CurrentRow.Cells["Domicilio"].Value.ToString();
        }
        #endregion
    }
}
