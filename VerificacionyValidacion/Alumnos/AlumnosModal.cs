﻿using LogicaNegocios.ControlEscolar;
using System;
using System.Windows.Forms;

namespace VerificacionyValidacion.Alumnos
{
    public partial class frmAlumnosModal : Form
    {
        private AlumnosManejador _AlumnosManejador;
        private EstadosManejador _EstadosManejador;
        private MunicipiosManejador _MunicipiosManejador;
        private Entidades.ControlEscolar.Alumnos _Alumnos;
        private bool Actualiza = false;

        public frmAlumnosModal()
        {
            InitializeComponent();
            _AlumnosManejador = new AlumnosManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _Alumnos = new Entidades.ControlEscolar.Alumnos();
        }
        public frmAlumnosModal(Entidades.ControlEscolar.Alumnos Alumno)
        {
            InitializeComponent();
            _AlumnosManejador = new AlumnosManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _Alumnos = new Entidades.ControlEscolar.Alumnos();
            _Alumnos = Alumno;
            Actualiza = true;
        }
        #region Acciones
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            if (!Actualiza)
            {
                Llenar();
                _AlumnosManejador.Guardar(_Alumnos);
                this.Close();
            }
            else
            {
                Llenar();
                _AlumnosManejador.Actualizar(_Alumnos);
                this.Close();
            }
        }

        private void FrmAlumnosModal_Load(object sender, EventArgs e)
        {
            LLenarEstados();
            if (Actualiza)
            {
                txtNoControl.Enabled = false;
                Rellenar();
                if (_Alumnos.Sexo.Equals("Masculino"))
                    cmbSexo.SelectedIndex = 0;
                else
                    cmbSexo.SelectedIndex = 1;
            }
        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            LLenarMunicipios(cmbEstado.SelectedIndex);
        }
        #endregion

        #region Metodos
        private void LLenarEstados()
        {
            var Estados = _EstadosManejador.RegresarEstados();
            foreach (var estado in Estados)
            {
                cmbEstado.Items.Add(estado.Estado);
            }
        }
        private void LLenarMunicipios(int estado)
        {
            cmbMunicipio.Items.Clear();
            int filtro = cmbEstado.SelectedIndex;
            var Municipios = _MunicipiosManejador.RegresarMunicipios(filtro + 1);
            foreach (var municipio in Municipios)
            {
                cmbMunicipio.Items.Add(municipio.Municipio);
            }
        }
        private void Llenar()
        {
            _Alumnos.NoControl = txtNoControl.Text;
            _Alumnos.Nombre = txtNombre.Text;
            _Alumnos.APaterno = txtAPaterno.Text;
            _Alumnos.AMaterno = txtAMaterno.Text;
            _Alumnos.Sexo = cmbSexo.Text;
            _Alumnos.Nacimiento = dtpNacimiento.Value.ToString();
            _Alumnos.Correo = txtCorreo.Text;
            _Alumnos.Telefono = txtTelefono.Text;
            _Alumnos.Estado = cmbEstado.Text;
            _Alumnos.Municipio = cmbMunicipio.Text;
            _Alumnos.Domicilio = txtDomicilio.Text;
        }
        private void Rellenar()
        {
            txtNoControl.Text = _Alumnos.NoControl;
            txtNombre.Text = _Alumnos.Nombre;
            txtAPaterno.Text = _Alumnos.APaterno;
            txtAMaterno.Text = _Alumnos.AMaterno;
            cmbSexo.Text = _Alumnos.Sexo;
            dtpNacimiento.Text = _Alumnos.Nacimiento;
            txtCorreo.Text = _Alumnos.Correo;
            txtTelefono.Text = _Alumnos.Telefono;
            cmbEstado.Text = _Alumnos.Estado;
            cmbMunicipio.Text = _Alumnos.Municipio;
            txtDomicilio.Text = _Alumnos.Domicilio;
        }
        #endregion
    }
}
