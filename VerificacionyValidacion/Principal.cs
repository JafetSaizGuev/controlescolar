﻿using System;
using System.Windows.Forms;

namespace VerificacionyValidacion
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlumnos A = new frmAlumnos();
            A.ShowDialog();
        }

        private void MaestrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Maestros.frmMaestros M = new Maestros.frmMaestros();
            M.ShowDialog();
        }

        private void escuelasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Escuelas.frmEscuelas E = new Escuelas.frmEscuelas();
            E.ShowDialog();
        }

        private void carrerasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Carreras.frmCarreras C = new Carreras.frmCarreras();
            C.ShowDialog();
        }

        private void materiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Materias.frmMaterias M = new Materias.frmMaterias();
            M.ShowDialog();
        }

        private void  gruposToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Grupos.frmGrupos G = new Grupos.frmGrupos();
            G.ShowDialog();
        }

        private void cargasAcademicasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CargasAcademicas.frmCargasAcademicas CA = new CargasAcademicas.frmCargasAcademicas();
            CA.ShowDialog();
        }

        private void salonesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Salones.frmSalones S = new Salones.frmSalones();
            S.ShowDialog();
        }

        private void respaldarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRespaldo R = new frmRespaldo();
            R.ShowDialog();
        }
    }
}
