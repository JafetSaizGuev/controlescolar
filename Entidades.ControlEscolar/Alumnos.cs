﻿namespace Entidades.ControlEscolar
{
    public class Alumnos
    {
        private string _NoControl;
        private string _Nombre;
        private string _APaterno;
        private string _AMaterno;
        private string _Sexo;
        private string _Nacimiento;
        private string _Correo;
        private string _Telefono;
        private string _Estado;
        private string _Municipio;
        private string _Domicilio;

        public string NoControl { get => _NoControl; set => _NoControl = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string APaterno { get => _APaterno; set => _APaterno = value; }
        public string AMaterno { get => _AMaterno; set => _AMaterno = value; }
        public string Sexo { get => _Sexo; set => _Sexo = value; }
        public string Nacimiento { get => _Nacimiento; set => _Nacimiento = value; }
        public string Correo { get => _Correo; set => _Correo = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Municipio { get => _Municipio; set => _Municipio = value; }
        public string Domicilio { get => _Domicilio; set => _Domicilio = value; }
    }
}
