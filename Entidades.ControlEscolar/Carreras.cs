﻿namespace Entidades.ControlEscolar
{
    public class Carreras
    {
        private string _Clave;
        private string _Carrera;
        private string _Encargado;

        public string Clave { get => _Clave; set => _Clave = value; }
        public string Carrera { get => _Carrera; set => _Carrera = value; }
        public string Encargado { get => _Encargado; set => _Encargado = value; }
    }
}
