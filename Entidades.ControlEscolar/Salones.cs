﻿namespace Entidades.ControlEscolar
{
    public class Salones
    {
        private int _Id;
        private string _Grupo;
        private string _Alumno;

        public int Id { get => _Id; set => _Id = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Alumno { get => _Alumno; set => _Alumno = value; }
    }
}
