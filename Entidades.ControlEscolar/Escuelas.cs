﻿namespace Entidades.ControlEscolar
{
    public class Escuelas
    {
        private int _ID;
        private string _Nombre;
        private string _Domicilio;
        private string _NumeroExterior;
        private string _Estado;
        private string _Municipio;
        private string _Telefono;
        private string _PaginaWeb;
        private string _DirectorActual;
        private string _Logo;

        public int ID { get => _ID; set => _ID = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string Domicilio { get => _Domicilio; set => _Domicilio = value; }
        public string NumeroExterior { get => _NumeroExterior; set => _NumeroExterior = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Municipio { get => _Municipio; set => _Municipio = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
        public string PaginaWeb { get => _PaginaWeb; set => _PaginaWeb = value; }
        public string DirectorActual { get => _DirectorActual; set => _DirectorActual = value; }
        public string Logo { get => _Logo; set => _Logo = value; }
    }
}
