﻿namespace Entidades.ControlEscolar
{
    public class Estados
    {
        private string _Estado;
        public string Estado { get => _Estado; set => _Estado = value; }
    }
    public class Municipios
    {
        private string _Municipio;
        public string Municipio { get => _Municipio; set => _Municipio = value; }
    }
}
