﻿namespace Entidades.ControlEscolar
{
    public class Grupos
    {
        private string _ID;
        private string _Carrera;
        private int _AnioEntrada;

        public string ID { get => _ID; set => _ID = value; }
        public string Carrera { get => _Carrera; set => _Carrera = value; }
        public int AnioEntrada { get => _AnioEntrada; set => _AnioEntrada = value; }
    }
}
