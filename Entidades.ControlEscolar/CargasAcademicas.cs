﻿namespace Entidades.ControlEscolar
{
    public class CargasAcademicas
    {
        private int _id;
        private string _Grupo;
        private string _Materia;

        public int Id { get => _id; set => _id = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
    }
}
