﻿namespace Entidades.ControlEscolar
{
    public class Maestros
    {
        private string _Codigo;
        private string _Nombre;
        private string _APaterno;
        private string _AMaterno;
        private string _Nacimiento;
        private string _Estado;
        private string _Municipio;
        private string _Sexo;
        private string _Correo;
        private string _Bancaria;

        public string Codigo { get => _Codigo; set => _Codigo = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string APaterno { get => _APaterno; set => _APaterno = value; }
        public string AMaterno { get => _AMaterno; set => _AMaterno = value; }
        public string Nacimiento { get => _Nacimiento; set => _Nacimiento = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Municipio { get => _Municipio; set => _Municipio = value; }
        public string Sexo { get => _Sexo; set => _Sexo = value; }
        public string Correo { get => _Correo; set => _Correo = value; }
        public string Bancaria { get => _Bancaria; set => _Bancaria = value; }
    }
    public class Estudios
    {
        private int _id;
        private string _MaestroID;
        private string _Archivo;

        public string MaestroID { get => _MaestroID; set => _MaestroID = value; }
        public string Archivo { get => _Archivo; set => _Archivo = value; }
        public int Id { get => _id; set => _id = value; }
    }
}
