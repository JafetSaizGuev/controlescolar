﻿namespace Entidades.ControlEscolar
{
    public class Materias
    {
        private string _Codigo;
        private string _Materia;
        private string _Antecesora;
        private string _Predecesora;

        public string Codigo { get => _Codigo; set => _Codigo = value; }
        public string Materia { get => _Materia; set => _Materia = value; }
        public string Antecesora { get => _Antecesora; set => _Antecesora = value; }
        public string Predecesora { get => _Predecesora; set => _Predecesora = value; }
    }
}
